<?php
require_once('conectar.php');
class CuentaSQL
{
	public function getconexion()
	{
		$conectarnos = new conectar();
       	return $conectarnos;		
	}

	public function Login($user, $password)
	{
		$procedimiento = $this->getconexion()->prepare("CALL Login(:user,:password)");
		$procedimiento->bindParam(":user",$user);	
		$procedimiento->bindParam(":password",$password);
		$procedimiento->execute();
		$result = $procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function ResetPassword($user, $newPassword)
	{
		$procedimiento = $this->getconexion()->prepare("CALL ChangeContra(:user,:password)");
		$procedimiento->bindParam(":user",$user);	
		$procedimiento->bindParam(":password",$newPassword);
		$procedimiento->execute();
		$actualizado = $procedimiento->rowCount();
		return $actualizado;
	}

}