<?php
require_once('conectar.php');
class EdificioSQL
{
	
	public function getconexion()
	{
		$conectarnos = new conectar();
       	return $conectarnos;		
	}
	
	public function InsertEdificio($nombre,$descripcion)
	{
		$procedimiento =$this->getconexion()->prepare('Call InsertEdificio(:nombre,:descripcion)');
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':descripcion',$descripcion);

		$procedimiento->execute();
		$insertado=$procedimiento->rowCount();
		return $insertado;
	}

	public function UpdateEdificio($idEdificio,$nombre,$descripcion)
	{
		$procedimiento =$this->getconexion()->prepare('Call UpdateEdificio(:idEdificio,:nombre,:descripcion)');
		$procedimiento->bindParam(':idEdificio',$idEdificio);
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':descripcion',$descripcion);
		$procedimiento->execute();
		$actualizado=$procedimiento->rowCount();
		return $actualizado;
	}
	
	public function DeleteEdificio($idEdificio)
	{
		
		$procedimiento =$this->getconexion()->prepare('Call DeleteEdificio(:idEdificio)');
		$procedimiento->bindParam(':idEdificio',$idEdificio);
		$procedimiento->execute();
		$eliminado=$procedimiento->rowCount();
		return $eliminado;
	}
}
