<?php
require_once "conectar.php";
class LaboratorioSQL
{
	public function getconexion()
	{
		$conectarnos = new conectar();
       	return $conectarnos;		
	}

	public function InsertLaboratorio($nombre,$descripcion,$idHorario,$idEdificio)
	{
		$procedimiento = $this->getconexion()->prepare('CALL InsertLaboratorio(:nombre,:descripcion,:idHorario,idEdificio)');
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':descripcion',$descripcion);
		$procedimiento->bindParam(':idHorario',$idHorario);
		$procedimiento->bindParam(':idEdificio'$idEdificio);

		$procedimiento->execute();
		$insertado = $procedimiento->rowCount();
		return $insertado;
	}

	public function UpdateLaboratorio($lab,$nombre,$descripcion,$horario,$edificio)
	{
		$procedimiento = $this->getconexion()->prepare('CALL UpdateLaboratorio (:lab,:nombre,:descripcion,:horario,:edificio)');
		$procedimiento->bindParam(':lab',$lab);
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':descripcion',$descripcion);
		$procedimiento->bindParam(':horario',$horario);
		$procedimiento->bindParam(':edificio',$edificio);

		$procedimiento->execute();
		$actualizado->procedimiento->rowCount();
		return $actualizado;
	}

	public function DeleteLaboratorio($idLab)
	{
		$procedimiento = $this->getconexion()->prepare('CALL DeleteLaboratorio (:idLaboratorio)');
		$procedimiento->bindParam(':idLaboratorio',$idLaboratorio);

		$procedimiento->execute();
		$eliminado = $procedimiento->rowCount();
		return $eliminado;
	}
}