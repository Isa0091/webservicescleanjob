<?php
require_once('conectar.php');
class UsuarioSQL
{
	public function getconexion()
	{
		$conectarnos = new conectar();
       	return $conectarnos;	
	}
	
	public function InsertUsers($idUser,$nombre,$apellido,$edad,$genero,$pass,$idTurno,$idTipo)
	{
		
	    $fechaActual = date("Y/m/d");
		$procedimiento =$this->getconexion()->prepare('Call InsertarUsers(:idUser,:nombre,:apellido,:edad,:genero,:pass,:fechaCreacion,:idTurno,:idTipo)');
		$procedimiento->bindParam(':idUser',$idUser);
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':apellido',$apellido);
		$procedimiento->bindParam(':edad',$edad);
		$procedimiento->bindParam(':genero',$genero);
		$procedimiento->bindParam(':pass',$pass);
		$procedimiento->bindParam(':fechaCreacion',$fechaActual);
		$procedimiento->bindParam(':idTurno',$idTurno);
		$procedimiento->bindParam(':idTipo',$idTipo);
		$procedimiento->execute();
		$insertado=$procedimiento->rowCount();
		return $insertado;
	}

	public function UpdateUsers($idUser,$nombre,$apellido,$edad,$genero,$pass,$idTurno,$idTipo)
	{
		
		$procedimiento =$this->getconexion()->prepare('Call UpdateUser(:idUser,:nombre,:apellido,:edad,:genero,:pass,:idTurno,:idTipo)');
		$procedimiento->bindParam(':idUser',$idUser);
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':apellido',$apellido);
		$procedimiento->bindParam(':edad',$edad);
		$procedimiento->bindParam(':genero',$genero);
		$procedimiento->bindParam(':pass',$pass);
		$procedimiento->bindParam(':idTurno',$idTurno);
		$procedimiento->bindParam(':idTipo',$idTipo);
		$procedimiento->execute();
		$actualizado=$procedimiento->rowCount();
		return $actualizado;
	}
	
	public function DeleteUsers($iduser)
	{
		$procedimiento =$this->getconexion()->prepare('Call DeleteUser(:iduser)');
		$procedimiento->bindParam(':iduser',$iduser);
		$procedimiento->execute();
		$eliminado=$procedimiento->rowCount();
		return $eliminado;
	}
	
	/* 
	public function selectcitasTodas(){
		
		$procedimiento =$this->getconexion()->prepare('Call sp_select_citas()');
		$procedimiento->execute();
		$listado_citas=$procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $listado_citas;
		

	}
	
	public function selectCitasPorFecha($fecha){
		
		$procedimiento =$this->getconexion()->prepare('Call sp_select_citasporFecha(:fecha)');
		$procedimiento->bindParam(':fecha',$fecha);
		
		$procedimiento->execute();
		$listado_citas_porFecha=$procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $listado_citas_porFecha;
		
	}
	
	public function selectcitasporRangoFecha($fechaini,$fechafini){
		
		$procedimiento =$this->getconexion()->prepare('Call sp_select_citasporRangoFecha(:fechaini,:fechafini)');
		$procedimiento->bindParam(':fechaini',$fechaini);
		$procedimiento->bindParam(':fechafini',$fechafini);
		
		$procedimiento->execute();
		$listado_citas_porRangoFecha=$procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $listado_citas_porRangoFecha;
	}
	 */
}
