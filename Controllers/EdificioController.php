<?php
require_once('Error.php');
require_once '../Services/EdificioService.php';
$EdificioService = new EdificioService();

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
	try 
	{
		validarEdificioPost();
		$nombre =$_POST["nombre"];
		$descripcion = $_POST["descripcion"];

		$rawdata = array();
		$rawdata=$EdificioService->InsertEdificio($nombre,$descripcion);
	   
	    if($rawdata ==1)
	    {
			$Status = new Status("Edificio","200","Edificio ingresado correctamente");
			echo json_encode($Status);
		}
		else
		{
			$Status = new Status("Edificio","401","Error al ingresar edificio");
			echo json_encode($Status);
		}
	}
	catch (Exception $e) 
	{ 
	    $Status = new Status("Edificio","401",$e->getMessage());
		echo json_encode($Status);
	}
}

if($_SERVER['REQUEST_METHOD'] === 'PUT')
{
	try 
	{
		validarEdificioPut();
		parse_str(file_get_contents("php://input"), $_PUT);
		
		$idEdificio =$_PUT["idEdificio"];
		$nombre =$_PUT["nombre"];
		$descripcion = $_PUT["descripcion"];
		
		$rawdata = array();
		$rawdata=$EdificioService->UpdateEdificio($idEdificio,$nombre,$descripcion);
	   
	    $Status = new Status("Edificio","200","Edificio actualizado correctamente");
		echo json_encode($Status);
	}
	catch (Exception $e) 
	{
       $Status = new Status("Edificio","401",$e->getMessage());
	   echo json_encode($Status);
    }
	
}

if($_SERVER['REQUEST_METHOD'] === 'DELETE')
{
	try 
	{
		parse_str(file_get_contents("php://input"), $_DELETE);
	
		if((isset($_DELETE["idEdificio"])) ==false || empty($_DELETE["idEdificio"]))
			throw new Exception("Debe enviar el identificador del edificio");
		
		$idEdificio =$_DELETE["idEdificio"];
		
		$rawdata = array();
		$rawdata=$EdificioService->DeleteEdificio($idEdificio);
		
		if($rawdata ==1)
		{
			$Status = new Status("Edificio","200","Edificio eliminado correctamente");
			echo json_encode($Status);
		}
		else
		{
			$Status = new Status("Edificio","401","Error al eliminar un edificio");
			echo json_encode($Status);
		}
	}
	catch (Exception $e) 
	{
		 
       $Status = new Status("Edificio","401",$e->getMessage());
	   echo json_encode($Status);
    }
}

 //Valida los parametros de los  edificios al momento de hacer las peticiones 
function validarEdificioPost()
{
	
	if((isset($_POST["nombre"]) && isset($_POST["descripcion"])) == false)
	{
		throw new Exception("Todos los campos son requeridos");
	}
	else if(empty($_POST["nombre"]) || empty($_POST["descripcion"]) )
	{
		throw new Exception("Todos los campos son requeridos");
	}

}

//valida el edificio del metodo put
function validarEdificioPut(){
	
	parse_str(file_get_contents("php://input"), $_PUT);
	
	if((isset($_PUT["idEdificio"]) && isset($_PUT["nombre"]) && isset($_PUT["descripcion"])) == false){
	
		  throw new Exception("Todos los campos son requeridos");
		
	}else if(empty($_PUT["idEdificio"]) || empty($_PUT["nombre"]) || empty($_PUT["descripcion"])){

		   throw new Exception("Todos los campos son requeridos");
	}
}
