<?php
require_once('Error.php');
require_once '../Services/CuentaService.php';
$CuentaService = new CuentaService();

if($_SERVER['REQUEST_METHOD'] === 'PUT')
{
	try 
	{
		if(!isset($_GET['user']) || !isset($_GET['password']))
		{
			throw new Exception("Todos los campos son requeridos");
		}
		
		$user = $_GET["user"];
		$newPassword = $_GET["password"];

		$rawdata = array();
		$rawdata=$CuentaService->ResetPassword($user, $newPassword);
	   
	    $Status = new Status("Cuenta","200","La contraseña se cambio con exito");
		echo json_encode($Status);
	}
	catch (Exception $e) 
	{
       $Status = new Status("Cuenta","401",$e->getMessage());
	   echo json_encode($Status);
    }
}

if($_SERVER['REQUEST_METHOD'] === 'GET')
{
	try
	{
		if(!isset($_GET['user']) || !isset($_GET['password']) )
			throw new Exception("Todos los campos son requeridos");

		$rawdata = $CuentaService->Login($_GET['user'],$_GET['password']);
		echo json_encode($rawdata);
	}
	catch(Exception $e)
	{
		$Status = new Status("Cuenta","401",$e->getMessage());
	   	echo json_encode($Status);
	}
}