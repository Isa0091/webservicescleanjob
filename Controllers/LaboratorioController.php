<?php
require_once('Error.php');
require_once '../Services/LaboratorioService.php';
$LabService = new LaboratorioService();

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
	try 
	{
		validarLaboratorioPost();
		$nombre = $_POST["nombre"];
		$descripcion = $_POST["descripcion"];
		$idHorario = $_POST["idHorario"];
		$idEdificio = $_POSt["idEdificio"];

		$rawdata = array();
		$rawdata=$LabService->InsertLaboratorio($nombre,$descripcion,$idHorario,$idEdificio);
	   
	    if($rawdata ==1)
	    {
			$Status = new Status("Laboratorio","200","Laboratorio ingresado correctamente");
			echo json_encode($Status);
		}
		else
		{
			$Status = new Status("Laboratorio","401","Error al ingresar laboratorio");
			echo json_encode($Status);
		}
	}
	catch (Exception $e) 
	{ 
	    $Status = new Status("Laboratorio","401",$e->getMessage());
		echo json_encode($Status);
	}
}

if($_SERVER['REQUEST_METHOD'] === 'PUT')
{
	try 
	{
		validarLaboratorioPut();
		parse_str(file_get_contents("php://input"), $_PUT);
		
		$lab = $_PUT["idLaboratorio"];
		$nombre = $_PUT["nombre"];
		$descripcion = $_PUT["descripcion"];
		$horario = $_PUT["idHorario"];
		$edificio = $_PUT["idEdificio"];
		
		$rawdata = array();
		$rawdata=$LabService->UpdateLaboratorio($lab,$nombre,$descripcion,$horario,$edificio);
	   
	    $Status = new Status("Laboratorio","200","Laboratorio actualizado correctamente");
		echo json_encode($Status);
	}
	catch (Exception $e) 
	{
       $Status = new Status("Laboratorio","401",$e->getMessage());
	   echo json_encode($Status);
    }
	
}

if($_SERVER['REQUEST_METHOD'] === 'DELETE')
{
	try 
	{
		parse_str(file_get_contents("php://input"), $_DELETE);
	
		if(!isset($_DELETE["idLaboratorio"]))
			throw new Exception("Debe enviar el identificador del laboratorio");
		
		$idLaboratorio =$_DELETE["idLaboratorio"];
		
		$rawdata = array();
		$rawdata=$LabService->DeleteLaboratorio($idLaboratorio);
		
		if($rawdata ==1)
		{
			$Status = new Status("Laboratorio","200","Laboratorio eliminado correctamente");
			echo json_encode($Status);
		}
		else
		{
			$Status = new Status("Laboratorio","401","Error al eliminar un Laboratorio");
			echo json_encode($Status);
		}
	}
	catch (Exception $e) 
	{
		 
       $Status = new Status("Laboratorio","401",$e->getMessage());
	   echo json_encode($Status);
    }
}

function validarLaboratorioPost()
{
	if(!isset($_POST["nombre"]) || !isset($_POST["descripcion"]) || !isset($_POST["idHorario"]) || !isset($_POSt["idEdificio"]))
		throw new Exception("Todos los campos son requeridos");
}

function validarLaboratorioPut()
{
	if(!isset($_PUT["idLaboratorio"]) || !isset($_PUT["nombre"]) || !isset($_PUT["descripcion"]) || !isset($_PUT["idHorario"]) || !isset($_PUT["idEdificio"]))
		throw new Exception("Todos los campos son requeridos");
}