<?php
require_once('../Data/LaboratorioSQL.php');
class LaboratorioService
{
	public function InsertLaboratorio($nombre,$descripcion,$idHorario,$idEdificio)
	{
		$LabSQL = new LaboratorioSQL(); 
		$rawdata = $LabSQL->InsertLaboratorio($nombre,$descripcion,$idHorario,$idEdificio);
		return ($rawdata);
	}

	public function UpdateLaboratorio($lab,$nombre,$descripcion,$horario,$edificio)
	{
		$LabSQL = new LaboratorioSQL();
		$rawdata = $LabSQL->UpdateLaboratorio($lab,$nombre,$descripcion,$horario,$edificio);
		return ($rawdata);
	}

	public function DeleteLaboratorio($idLab)
	{
		$LabSQL = new LaboratorioSQL();
		$rawdata = $LabSQL->DeleteLaboratorio($idLab);
		return ($rawdata);
	}
}
	