<?php
require_once('../Data/UsuarioSQL.php');
class UsuarioService
{
	public function InsertUsers($idUser,$nombre,$apellido,$edad,$genero,$pass,$idTurno,$idTipo)
	{
		$UserSQL = new UsuarioSQL(); 
		$rawdata=$UserSQL->InsertUsers($idUser,$nombre,$apellido,$edad,$genero,$pass,$idTurno,$idTipo);
		return ($rawdata);
	}
	
	public function UpdateUsers($idUser,$nombre,$apellido,$edad,$genero,$pass,$idTurno,$idTipo)
	{
		$UserSQL = new UsuarioSQL(); 
		$rawdata=$UserSQL->UpdateUsers($idUser,$nombre,$apellido,$edad,$genero,$pass,$idTurno,$idTipo);
		return ($rawdata);
	}
	
	public function DeleteUsers($idUser)
	{
		$UserSQL = new UsuarioSQL(); 
		$rawdata=$UserSQL->DeleteUsers($idUser);
		return ($rawdata);	
	}
	
	/* 	
	public function selectcitasTodas(){
		
		$citas = new Operaciones_Citas(); 
		$rawdata=$citas->selectcitasTodas();
		return ($rawdata);
		
	}
	
	public function selectCitasPorFecha($fecha){
		
	   $citas = new Operaciones_Citas(); 
		$rawdata=$citas->selectCitasPorFecha($fecha);
		return ($rawdata);
		
	}
	
	public function selectcitasporRangoFecha($fechaini,$fechafini){
		 $citas = new Operaciones_Citas(); 
		$rawdata=$citas->selectcitasporRangoFecha($fechaini,$fechafini);
		return ($rawdata);
		
	} */
}