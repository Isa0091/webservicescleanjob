<?php
require_once('../Data/EdificioSQL.php');
class EdificioService
{

	public function InsertEdificio($nombre,$descripcion)
	{
		$EdificioSQL = new EdificioSQL(); 
		$rawdata=$EdificioSQL->InsertEdificio($nombre,$descripcion);
		return ($rawdata);
	}
	
	public function UpdateEdificio($idEdificio,$nombre,$descripcion)
	{	
		$EdificioSQL = new EdificioSQL(); 
		$rawdata=$EdificioSQL->UpdateEdificio($idEdificio,$nombre,$descripcion);
		return ($rawdata);	
	}
	
	public function DeleteEdificio($idEdificio)
	{
		$EdificioSQL = new EdificioSQL(); 
		$rawdata=$EdificioSQL->DeleteEdificio($idEdificio);
		return ($rawdata);
	}
}
