<?php
require_once "../Data/CuentaSQL.php";
class CuentaService
{
	public function Login($user, $password)
	{
		$CuentaSQL = new CuentaSQL(); 
		$rawdata = $CuentaSQL->Login($user,$password);
		return ($rawdata);
	}

	public function ResetPassword($user, $newPassword)
	{
		$CuentaSQL = new CuentaSQL();
		$rawdata = $CuentaSQL->ResetPassword($user,$newPassword);
		return ($rawdata);
	}
}